<?php
require_once ('vendor/autoload.php');

use PhpAmqpLib\Connection\AMQPStreamConnection;
use App\RabbitMQConfiguration;
use App\EmailSenderFactory;



$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
$channel = $connection->channel();

$connection = new AMQPStreamConnection(
    RabbitMQConfiguration::HOST,
    RabbitMQConfiguration::PORT,
    RabbitMQConfiguration::USER,
    RabbitMQConfiguration::PASSWORD
);

list($queue_name, ,) = $channel->queue_declare("", false, false, true, false);

$channel->queue_bind($queue_name, RabbitMQConfiguration::EXCHANGE);

echo " [*] Waiting for logs. To exit press CTRL+C\n";

$callback = function ($msg) {
    $payload = json_decode($msg->body, true);
    $senderType = $payload['sender'];
    $senderClass = new EmailSenderFactory;
    $sender = $senderClass($senderType);
    $sender(
        $payload['email'],
        $payload['subject'],
        $payload['message']
    );

};

$channel->basic_consume($queue_name, '', false, true, false, false, $callback);

while ($channel->is_open()) {
    $channel->wait();
}

$channel->close();
$connection->close();
