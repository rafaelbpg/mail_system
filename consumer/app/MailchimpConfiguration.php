<?php

namespace App;

abstract class MailchimpConfiguration
{
    const API_KEY = '';
    const SENDER = '';
    const SENDER_NAME = '';
}