<?php declare( strict_types = 1 );

namespace App;

interface EmailSenderInterface{
    public function __invoke(string $emailAddress, string $subject, string $message): void;
}