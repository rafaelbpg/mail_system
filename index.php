<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Email Sender</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <script defer src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script defer src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <script defer src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        const sendEmail = button => {
            let form = button.closest('form');
            if (!form.checkValidity()){
                errorInFields();
                return 0;
            }
        
            let email = form.email.value;
            let subject = form.subject.value;
            let message = form.message.value;
            let sender = form.sender.value;
            let payload = {
                email:email,
                subject:subject,
                message:message,
                sender:sender
            };
            fetch('producer.php', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(payload)
            })
            .then(response => {
                emailSentProperly();
            })
            .catch(error => {
                emailSendingFailed();
            })
        }
        const errorInFields = _ => {
            Swal.fire({
                title:'Oops! Something was wrong...',
                html:'You must to fill properly the fields in the form',
                icon:'error'
            })
        }
        const emailSentProperly = _ => {
            Swal.fire({
                title:'Success',
                html:'The e-mail has been sent',
                icon:'success'
            })
        }
        const emailSendingFailed = _ => {
            Swal.fire({
                title:'Error',
                html:'Something was wrong, try again',
                icon:'error'
            })
        }
    </script>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>
                    Email Sender With RabbitMQ
                </h1>
            </div>
        </div>
        <form class="row border bg-light">
            <div class="col-12">
                <label>
                    Select the e-mail sender
                </label>
                <br/>
                <input type="radio" name="sender" value="DEFAULT" checked>Default PHP sender
                <br/>
                <input type="radio" name="sender" value="MAILCHIMP">Mailchimp
            </div>
            <div class="col-12 mt-2">
                <label for="email">E-Mail</label>
                <input type="email" name="email" class="form-control" required />
            </div>
            <div class="col-12">
                <label for="subject">Subject</label>
                <input type="text" name="subject" class="form-control" required />
            </div>
            <div class="col-12">
                <label for="message">Message</label>
                <textarea name="message" class="form-control" required></textarea>
            </div>
            <div class="col-12 text-center mt-2">
                <button type="reset" class="btn btn-secondary">Reset form</button>
                <button type="button" class="btn btn-primary" onclick="sendEmail(this);">Send e-mail</button>
            </div>
        </form>
    </div>
</body>
</html>