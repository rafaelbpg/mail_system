<?php declare( strict_types = 1);

namespace App;

final class EmailSenderFactory{
    const EMAIL_SENDER = [
        'DEFAULT' => DefaultMailSender::class,
        'MAILCHIMP' => MailchimpSender::class
    ];
    public function __invoke(string $senderName) : EmailSender
    {
        $classSender = self::EMAIL_SENDER[$senderName] ?? false;
        if (!$classSender) {
            die('UNDEFINED CLASS ' . $senderName );
        }
        return new $classSender;
    }
}
