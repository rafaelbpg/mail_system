<?php

namespace App;

abstract class RabbitMQConfiguration
{
    const HOST = 'localhost';
    const PORT = '5672';
    const USER = 'guest';
    const PASSWORD = 'guest';

    const EXCHANGE = 'mail_exchange';
    const TYPE = 'fanout';
    const PASSIVE = false;
    const DURABLE = false;
    const AUTO_DELETE = false;

}