<?php declare( strict_types = 1);

namespace App;

abstract class EmailSender implements EmailSenderInterface{
    abstract function __invoke(string $emailAddress, string $subject, string $message): void;
}