<?php declare( strict_types = 1);

namespace App;

final class DefaultMailSender extends EmailSender {
    public function __invoke(string $emailAddress, string $subject, string $message): void {
        $message = wordwrap($message, 70, "\r\n");
        mail($emailAddress, $subject, $message);
        echo "ENVIADOR POR DEFAULT";
    }
}