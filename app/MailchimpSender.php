<?php declare( strict_types = 1);

namespace App;
use App\MailchimpConfiguration;
use MailchimpTransactional\ApiClient;

final class MailchimpSender extends EmailSender {
    public function __invoke(string $emailAddress, string $subject, string $message): void {
        $mailchimp = new ApiClient();
        $mailchimp->setApiKey(MailchimpConfiguration::API_KEY);
        $response = $mailchimp->messages->send(["message" => [
            'html' => $message,
            'subject' => $subject,
            'from_email' => MailchimpConfiguration::SENDER,
            'from_name' => MailchimpConfiguration::SENDER_NAME,
            'to' => [[
                'email' => $emailAddress,
                'name' => 'Rafael Perez',
                'type' => 'to'
            ]],
            'important' => true,
            'track_opens' => true,
            'track_clicks' => true,
        ]]);
        echo "ENVIADO USANDO MAILCHIMP";
    }
}