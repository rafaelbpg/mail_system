<?php
$host = 'localhost';
$dbname = 'x';
$usuario = 'root';
$contraseña = '';


$mdb = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8", $usuario, $contraseña);
/*
    a) Dado un id de cliente, necesitamos saber en qué tiendas ha comprado
*/
$cliente_id = 1;
$query = "SELECT DISTINCT(tienda_id) FROM compras WHERE cliente_id = :cliente_id";
$stmt = $mdb->prepare($query);
$stmt->execute([':cliente_id' => $cliente_id]);
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
echo "El cliente {$cliente_id} ha comprado en las tiendas: ";
print_r($result);
/* 
    b) Dada una id de tienda, queremos ordenar las compras del último mes por total
    comprado
*/
$tienda_id = 1;
$query = "SELECT * FROM compras WHERE tienda_id = :tienda_id AND fecha >= DATE_FORMAT( CURRENT_DATE - INTERVAL 1 MONTH, '%Y/%m/%d' ) 
AND fecha < DATE_FORMAT( CURRENT_DATE + INTERVAL 1 DAY, '%Y/%m/%d' ) ORDER BY total ASC";
$stmt = $mdb->prepare($query);
$stmt->execute([':tienda_id' => $tienda_id]);
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
echo "Compras ordenadas por total ASC: ";
print_r($result);

$tienda_id = 1;
$query = "SELECT * FROM compras WHERE tienda_id = :tienda_id AND fecha >= DATE_FORMAT( CURRENT_DATE - INTERVAL 1 MONTH, '%Y/%m/%d' ) 
AND fecha < DATE_FORMAT( CURRENT_DATE + INTERVAL 1 DAY, '%Y/%m/%d' ) ORDER BY total DESC";
$stmt = $mdb->prepare($query);
$stmt->execute([':tienda_id' => $tienda_id]);
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
echo "Compras ordenadas por total DESC: ";
print_r($result);

/* c) Queremos saber en orden descendente las tiendas que más venden en número de
ventas y total */
$query = "SELECT tienda_id, SUM(total) as total_ventas FROM compras GROUP BY (tienda_id) ORDER BY total_ventas DESC";
$stmt = $mdb->prepare($query);
$stmt->execute();
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
echo "Las tiendas que más venden en orden descendiente total son: ";
print_r($result);

$query = "SELECT tienda_id, COUNT(id) as numero_de_ventas FROM compras GROUP BY (tienda_id) ORDER BY numero_de_ventas DESC";
$stmt = $mdb->prepare($query);
$stmt->execute();
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
echo "Las tiendas que más venden en orden descendiente en número de ventas son: ";
print_r($result);
/* 
    d) Queremos saber en qué tiendas compra la gente de un código postal en concreto
*/
$codigoPostal = 43511;
$query = "SELECT DISTINCT(compras.tienda_id) FROM compras 
INNER JOIN clientes 
ON compras.cliente_id = clientes.id
WHERE clientes.codigo_postal = :codigo_postal";
$stmt = $mdb->prepare($query);
$stmt->execute([':codigo_postal' => $codigoPostal]);
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
echo "La gente que tiene código postal igual a {$codigoPostal}  compra en las siguientes tiendas: ";
print_r($result);
/*
    e) Queremos saber cuál es el producto que más se compra en cada tienda
*/
$codigoPostal = 43511;
$query = "SELECT *, MAX(total) 
    FROM (
        SELECT 
            compralineas.producto_id as producto_id, 
            SUM(compralineas.unidades) as total, 
            compras.tienda_id as tienda_id 

        FROM 
            compralineas 
        INNER JOIN 
            compras 
        ON 
            compralineas.compra_id = compras.id
        GROUP BY 
            tienda_id, producto_id) 
        sales
    GROUP BY
        tienda_id

";
echo $query;
$stmt = $mdb->prepare($query);
$stmt->execute();
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
echo "Los productos más comprados por tienda son: ";
print_r($result);