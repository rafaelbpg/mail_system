Ejecutar composer install.
Hacer un php consumer.php (al que está en el raíz del proyecto) para ejecutar el consumer.
Entrar desde un navegador web al index.php. 
Ahí aparecerá un formulario para enviar mail. 
Si se seleccion default PHP sender se enviará el e-mail
con la función mail de PHP (asumimos que ya está configurada)
en el entorno local, si se selecciona Mailchimp se enviará
utilizando la API de Mailchimp —hay que configurar las variables en
el fichero app/MailchimpConfiguration.php.
Para generar este comportamiento he utilizado el patrón Factory.
Si se quisiera hacer un cambio manual y eliminar esto, bastaría
con meter una variable de configuración y tocar en un solo sitio.
En app/RabbitMQConfiguration.php están algunas variables de configuración
básica de RabbitMQ, pero damos por hecho que está con los valores por defecto.

Index.php llama al producer.php al darle al enviar mail. Dividir esto
a servicios más pequeños en distintos servidores no es tarea complicada.

Dejo unas carpetas para producer y consumer dónde se podrían llevar fácilmente
a sitios diferentes. Sólo habría que cambiar el endpoint de index.php.

