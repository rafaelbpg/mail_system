<?php

require_once('vendor/autoload.php');

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use App\RabbitMQConfiguration;

$connection = new AMQPStreamConnection(
    RabbitMQConfiguration::HOST,
    RabbitMQConfiguration::PORT,
    RabbitMQConfiguration::USER,
    RabbitMQConfiguration::PASSWORD
);

$channel = $connection->channel();

$channel->exchange_declare(
    RabbitMQConfiguration::EXCHANGE,
    RabbitMQConfiguration::TYPE,
    RabbitMQConfiguration::PASSIVE,
    RabbitMQConfiguration::DURABLE,
    RabbitMQConfiguration::AUTO_DELETE
);
//rafaeltestignoraresteemail
//rafaeltest123
$msg = new AMQPMessage(file_get_contents('php://input'));
$channel->basic_publish($msg, 'mail_exchange');
$channel->close();
$connection->close();